﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DZ_Regex_winForms
{
    public static class FileSaver
    {
        public static void DownLoadFiles(List<string> imgPaths, string filesFolder)
        {
            if (!System.IO.Directory.Exists(filesFolder))
            {
                throw new DirectoryNotFoundException("Некорректный путь к папке");
            }
            var client = new WebClient();
            foreach (var imgPath in imgPaths)
            {
                var fileName = imgPath.Substring(imgPath.LastIndexOf("/"));

                var filePath = filesFolder + fileName;

                try
                {    
                    client.DownloadFile(imgPath, filePath);
                }
                catch(Exception ex)
                {
                    throw new HttpRequestException("Не корректный путь в картинке");
                }
            }
        }
    }
}
