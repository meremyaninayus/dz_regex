﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DZ_Regex_winForms
{
    public static class HtmlParser
    {
        public static List<string> ParseUrl(string url)
        {
            var siteAsStr = DownLoadSiteData(url);
            return ParseData(siteAsStr, url);
        }

        private static List<string> ParseData(string data, string url)
        {
            var imgPaths = new List<string>();
            var regex = new Regex(@"src(set)?=""(.+?)\.(png|jpg|jpeg|ico|gif)");
            var matchs = regex.Matches(data);
            foreach (Match match in matchs)
            {
                var firstGroup = match.Groups[0];
                var imgPath = firstGroup.ToString().Substring(firstGroup.ToString().IndexOf("\""));

                imgPath = imgPath.Replace("\"", "").Replace("?", "");

                if (!imgPath.StartsWith("http"))
                {
                    imgPath = url + imgPath;
                }

                imgPaths.Add(imgPath);
            }
            return imgPaths;
        }
        private static string DownLoadSiteData(string url)
        {
            var correctUri = new Uri(url, UriKind.Absolute);
            var client = new WebClient();
            return client.DownloadString(correctUri);
        }
    }
}
