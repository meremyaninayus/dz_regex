﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DZ_Regex_winForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var res = folderBrowserDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var filesFolder = textBox2.Text;
            if (!System.IO.Directory.Exists(filesFolder))
            {
                MessageBox.Show("Некорректный путь к папке");
                return;
            }

            var url = textBox1.Text;
            try
            {
                var paths = HtmlParser.ParseUrl(url);
                FileSaver.DownLoadFiles(paths, filesFolder);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            MessageBox.Show("Картинки успешно скопированы");
        }


    }
}
